const API_BASE_URL = 'http://localhost:3000/testApi';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const getUsers = async () => {
  try {
    const response = await fetch(`${API_BASE_URL}/users`, {
      method: 'GET',
      headers,
    });
    const content = await response.json();
    return content.data;
  } catch (error) {
    return { error };
  }
};

export const addUser = async (body) => {
  try {
    const response = await fetch(`${API_BASE_URL}/users`, {
      method: 'POST',
      headers,
      body: JSON.stringify({ ...body }),
    });
    const content = await response.json();
    return { response: content };
  } catch (error) {
    return { error };
  }
};

export const deleteUser = async (userId) => {
  try {
    const response = await fetch(`${API_BASE_URL}/users/${userId}`, {
      method: 'DELETE',
      headers,
    });
    const content = await response.json();
    return { response: content };
  } catch (error) {
    return { error };
  }
};

export const getUser = async (userId) => {
  try {
    const response = await fetch(`${API_BASE_URL}/users/${userId}`, {
      method: 'GET',
      headers,
    });
    const content = await response.json();
    return content.data;
  } catch (error) {
    return { error };
  }
};

export const updateUser = async (user) => {
  try {
    const response = await fetch(`${API_BASE_URL}/users/${user.id}`, {
      method: 'PUT',
      headers,
      body: JSON.stringify({ ...user }),
    });
    const content = await response.json();
    return content.data;
  } catch (error) {
    return { error };
  }
};
