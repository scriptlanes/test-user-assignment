import * as Yup from 'yup';

export const AddUserSchema = () => {
  return Yup.object().shape({
    userName: Yup.string().required('User name is required'),
    givenName: Yup.string(),
    surName: Yup.string(),
    DOB: Yup.date(),
  });
};
