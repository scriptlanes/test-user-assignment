import { Button, Form, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import { AddUserSchema } from '../shared/AddUserSchema';
import { addUser, updateUser } from '../axios';
import { useState } from 'react';
import NxToast from './Toast';

const AddUser = ({
  user,
  showAddUserModal,
  handleAddUserModal,
  getAllUsers,
  setUsers,
}) => {
  const [toast, setToast] = useState(false);
  const [toastMessage, setToastMessage] = useState('');

  /**
   * Add or Update the existing user
   * @param {object} value User form values
   */
  const handleSubmit = (value) => {
    if (Object.keys(user).length) {
      updateUser({ ...user, ...value })
        .then(() => {
          setUsers({});
          handleAddUserModal();
          setToastMessage('User updated successfully');
          setToast(true);
          getAllUsers();
        })
        .catch((err) => console.error(err));
    } else {
      addUser(value)
        .then(() => {
          handleAddUserModal();
          setToastMessage('New user added successfully');
          setToast(true);
          getAllUsers();
        })
        .catch((err) => console.error(err));
    }
  };

  return (
    <>
      <Modal show={showAddUserModal} onHide={handleAddUserModal}>
        <Formik
          initialValues={
            user || { userName: '', givenName: '', surName: '', DOB: '' }
          }
          onSubmit={(values) => handleSubmit(values)}
          validationSchema={AddUserSchema}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
            } = props;
            return (
              <>
                <Modal.Header closeButton>
                  <Modal.Title>Add New User</Modal.Title>
                </Modal.Header>

                <Form noValidate onSubmit={handleSubmit}>
                  <Modal.Body>
                    <Form.Group className="mb-3" controlId="userName">
                      <Form.Label>User name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter user name"
                        name="userName"
                        value={values.userName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        required
                        className={
                          errors.userName && touched.userName
                            ? 'border border-danger'
                            : ''
                        }
                      />
                      {errors.userName && touched.userName && (
                        <small className="text-danger">{errors.userName}</small>
                      )}
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="givenName">
                      <Form.Label>Given name</Form.Label>
                      <Form.Control
                        type="text"
                        name="givenName"
                        placeholder="Enter given name"
                        value={values.givenName}
                        onChange={handleChange}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="surName">
                      <Form.Label>surName</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter surname"
                        name="surName"
                        value={values.surName}
                        onChange={handleChange}
                        isInvalid={!!errors.surName}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="DOB">
                      <Form.Label>Date of Birth</Form.Label>
                      <Form.Control
                        type="date"
                        placeholder="Date of Birth"
                        name="DOB"
                        value={values.DOB}
                        onChange={handleChange}
                        isInvalid={!!errors.DOB}
                      />
                    </Form.Group>
                  </Modal.Body>

                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleAddUserModal}>
                      Close
                    </Button>

                    <Button
                      type="submit"
                      variant="primary"
                      disabled={isSubmitting}
                    >
                      Submit
                    </Button>
                  </Modal.Footer>
                </Form>
              </>
            );
          }}
        </Formik>
      </Modal>
      {toast && (
        <NxToast
          variant="success"
          title="Success"
          message={toastMessage}
          show={toast}
          setToast={setToast}
        />
      )}
    </>
  );
};

export default AddUser;
