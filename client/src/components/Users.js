import React, { useState, useEffect } from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import { deleteUser, getUser, getUsers } from '../axios';
import AddUser from './AddUser';
import NxToast from './Toast';

const Users = () => {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});
  const [toast, setToast] = useState(false);

  const [showAddUserModal, setShowAddUserModal] = useState(false);

  const handleAddUserModal = () =>
    setShowAddUserModal((prevState) => !prevState);

  useEffect(() => {
    getAllUsers();
    return () => {};
  }, []);

  /**
   * Get all users
   */
  const getAllUsers = () => {
    getUsers()
      .then((res) => {
        setUsers(res.userData);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  /**
   * Delete the existing user
   * @param {object} event Click event to stop event propagation
   * @param {string} userId User ID of the user
   */
  const handleDeleteUser = (event, userId) => {
    event.stopPropagation();
    deleteUser(userId)
      .then(() => {
        getAllUsers();
        setToast(true);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  /**
   * Get user information using user id
   * @param {string} userId User ID of the user
   */
  const handleUpdate = (userId) => {
    getUser(userId)
      .then((res) => {
        setUser(res.userData);
        handleAddUserModal();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  return (
    <>
      {users.length ? (
        users.map((user) => {
          return (
            <ListGroup as="ul" key={user.id}>
              <ListGroup.Item
                as="li"
                className="d-flex justify-content-between align-items-center"
                action
                onClick={() => handleUpdate(user.id)}
              >
                <div className="ms-2 me-auto">
                  <div className="fw-bold">
                    {user.givenName} {user.surName}
                  </div>
                  {user.userName} ( {user.DOB} )
                </div>
                <Button
                  variant="danger"
                  size="sm"
                  onClick={(e) => handleDeleteUser(e, user.id)}
                >
                  Delete
                </Button>
              </ListGroup.Item>
            </ListGroup>
          );
        })
      ) : (
        <h4 className="text-center">No data found!</h4>
      )}

      {toast && (
        <NxToast
          variant="success"
          title="Success"
          message="User deleted successfully"
          show={toast}
          setToast={setToast}
        />
      )}

      <AddUser
        showAddUserModal={showAddUserModal}
        handleAddUserModal={handleAddUserModal}
        getAllUsers={getAllUsers}
        user={user}
        setUsers={setUsers}
      />
      <Button className="mt-5" variant="primary" onClick={handleAddUserModal}>
        Add New User
      </Button>
    </>
  );
};

export default Users;
