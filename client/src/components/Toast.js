import { Toast, ToastContainer } from 'react-bootstrap';

const NxToast = ({ show, setToast, variant, title, message }) => {
  return (
    <div aria-live="polite" aria-atomic="true" className="position-relative">
      <ToastContainer position="top-end" className="p-3">
        <Toast
          onClose={() => setToast(false)}
          bg={variant.toLowerCase()}
          show={show}
          delay={1000}
          autohide
        >
          <Toast.Header>
            <strong className="me-auto">{title}</strong>
          </Toast.Header>
          <Toast.Body className="text-white">{message}</Toast.Body>
        </Toast>
      </ToastContainer>
    </div>
  );
};

export default NxToast;
