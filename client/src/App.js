import Users from './components/Users';

const App = () => {
  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col">
          <Users />
        </div>
      </div>
    </div>
  );
};
export default App;
