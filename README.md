# TestUserApp
Test Interview React and Node JS test

**Problem Statement**

*Technical Sample Application*

*Using Node.js and React create a set of REST services that can be used to manage a
User class. The user object shall contain the following properties:* 
```JSON
{
    "userObj": {
        "type": "object",
        "properties": {
            "id": {
                "type": "guid",
                "required": true
                },
            "userName": {
                "type": "string",
                "required": true
                },
            "givenName": {
                "type": "string",
                "required": false
            },
            "surName": {
                "type": "string",
                "required": false
            },
            "DOB": {
                "type": "string",
                "required": false
            }
        }
    }
}
```

*The most important success factor is to produce the best solution you can to
solve the problem. There are many aspects to consider within that context -
efficiency, performance, documentation,etc. Code to perform the algorithm is
the minimum requirement.* 



*Your deliverable should contain enough to give us a
glimpse into your analysis, your approach, your skills, or anything else
that will demonstrate your ability as a software developer. We will be
looking for a good approach, a glimpse into your thought process, things you
can do to impress us, and clean and efficient code.*

*Your deliverable will be something that we can execute (to test the
results) along with whatever explanation of the code/program that you feel
is necessary.*  





## Developer Updates


## About the app
Actually, there are two separated apps. The Client which serves the FrontEnd (using React), and the API (in Node/Express/Prisma/Swagger/Sqlite).

## How to run the API ( Node App )
1. In your terminal, navigate to the `api` directory.
2. Run `npm install` to install all dependencies.
3. Run `npm run dev` to start the app. API will start at [http://localhost:3000](http://localhost:3000)
4. API documentation done using Swagger at [http://localhost:3000/api-docs](http://localhost:3000/api-docs)
5. API using [prima.io](https://www.prisma.io) for ORM and sqlite for DB

## How to run the Client ( React App )
1. In another terminal, navigate to the `client` directory.
2. Run `npm install` to install all dependencies.
3. Run `npm run start` to start the app, when prompted another app is running on same port and you want to use another port type "Y"
4. React App will start at [http://localhost:3001](http://localhost:3001). You can Create , List , Delete and Update Users
5. Code level comments are added for User and Add User components

