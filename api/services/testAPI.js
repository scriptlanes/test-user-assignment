var userService = {};
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

userService.createUser = async (user) => {
    try {
        const userData = await prisma.user.create({ data: user });

        return { userData: userData };
    } catch (err) {
        return (err);
    }

}

userService.getAllUsers = async () => {
    try {
        const userData = await prisma.user.findMany();

        return { userData: userData };
    } catch (err) {
        return (err);
    }

}

userService.getUserById = async (userId) => {
    try {
        console.log("id", userId);
        const userData = await prisma.user.findFirst({where: {id:Number(userId)}});
        return { userData: userData };
    } catch (err) {
        return (err);
    }

}

userService.updateUser = async (userId,userData) => {
    try {
        const updateUserData = await prisma.user.update({ where: { id: Number(userId) }, data: { ...userData} });

        return { userData: updateUserData };
    } catch (err) {
        return (err);
    }

}

userService.delete = async (userId) => {
    try {
        const userData = await prisma.user.delete({where: {id:Number(userId)}});
        return { userData: userData };
    } catch (err) {
        return (err);
    }

}


module.exports = userService;