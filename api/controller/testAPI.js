var userController = {};
var userService = require("../services/testAPI")
const { validationResult } = require('express-validator/check');

userController.addUser = async function (req, res,next) {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
          res.status(409).json({ errors: errors.array() });
          return;
        }
        var user = req.body;

        var userData = await userService.createUser(user);

        res.status(200).json({ data: userData, message: 'User created' });
    } catch (err) {
        next(err);
    }
}

userController.getAllUsers = async function (req, res,next) {
    try {
        var userData = await userService.getAllUsers();

        res.status(200).json({ data: userData, message: 'find All Users' });
    } catch (err) {
        next(err);
    }
}

userController.getUserById = async function (req, res,next) {
    try {
        const userId = req.params.userId;
        var userData = await userService.getUserById(userId);

        res.status(200).json({ data: userData, message: 'find one User' });
    } catch (err) {
        next(err);
    }
}

userController.updateUser = async function (req, res,next) {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
          res.status(409).json({ errors: errors.array() });
          return;
        }
        var user = req.body;
        var userId = req.params.id;
        var userData = await userService.updateUser(userId,user);

        res.status(200).json({ data: userData, message: 'User updated' });
    } catch (err) {
        next(err);
    }
}

userController.deleteUser = async function (req, res,next) {
    try {
        const userId = req.params.id;
        var userData = await userService.delete(userId);

        res.status(200).json({ data: userData, message: 'user deleted' });
    } catch (err) {
        next(err);
    }
}

module.exports = userController;