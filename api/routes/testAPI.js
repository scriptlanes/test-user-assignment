var express = require("express");
var router = express.Router();
const userController = require("../controller/testAPI")
const validator = require("../middleware/validator")
const path = '/users';
/** 
 * @swagger 
 * /testAPI/users:
 *   post: 
 *     description: Use to add new user
 *     responses:  
 *       200: 
 *         description: Success  
 *       409: 
 *         description: Conflict 
 *   
 */ 
router.post(`${path}`,validator.validate('createUser'), userController.addUser)
/** 
 * @swagger 
 * /testAPI/users:
 *   get: 
 *     description: Use to get all user
 *     responses:  
 *       200: 
 *         description: Success  
 *   
 */ 
router.get(`${path}`, userController.getAllUsers);
/** 
 * @swagger 
 * /testAPI/users/{userId}:
 *   get: 
 *     description: Use to get one user
 *     responses:  
 *       200: 
 *         description: Success  
 *       409: 
 *         description: Conflict 
 *   
 */ 
router.get(`${path}/:userId`,validator.validate('getOne'), userController.getUserById);
/** 
 * @swagger 
 * /testAPI/users/{id}:
 *   put: 
 *     description: Use to update user
 *     responses:  
 *       200: 
 *         description: Success  
 *       409: 
 *         description: Conflict 
 *   
 */ 
router.put(`${path}/:id`,validator.validate('createUser'), userController.updateUser)
/** 
 * @swagger 
 * /testAPI/users/{id}:
 *   delete: 
 *     description: Use to delete user
 *     responses:  
 *       200: 
 *         description: Success  
 *       409: 
 *         description: Conflict 
 *   
 */
router.delete(`${path}/:id`,validator.validate('getOne'), userController.deleteUser)
module.exports = router;
