-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "userName" TEXT NOT NULL,
    "givenName" TEXT,
    "surName" TEXT,
    "DOB" TEXT
);
