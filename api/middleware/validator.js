const { body,param } = require('express-validator/check')

exports.validate = (method) => {
  switch (method) {
    case 'createUser': {
     return [ 
        body('userName', 'userName is required').not().isEmpty().trim().escape()
       ]   
    }
    case 'getOne': {
        return [ 
           param('userId', 'userName is required').not().isEmpty().trim().escape()
          ]   
       }
  }
}